<h2 align="center">Technical DS test for Servier</h2>
<br>

## About The Project

### Built With

* [Python](https://www.python.org/)
* [Tensorflow](https://www.tensorflow.org/)
* [Keras](https://keras.io/)
* [scikit-learn](https://scikit-learn.org/)
* [Docker](https://www.docker.com/)

<p align="right">(<a href="#top">back to top</a>)</p>
<br>

### Organisation of the project

```bash
├── app
│   ├── __init__.py
│   ├── api_app.py
│   └── main.py
├── model
│   ├── model1
│   └── model2
├── src
│   ├── evaluate
│   │   ├── __init__.py
│   │   └── evaluate_model.py
│   ├── gathering
│   │   ├── __init__.py
│   │   └── get_data.py
│   ├── predict
│   │   ├── __init__.py
│   │   └── predict.py
│   ├── preprocessing
│   │   ├── __init__.py
│   │   ├── feature_extractor.py
│   │   └── tokenize.py
│   ├── training
│   │   ├── __init__.py
│   │   ├── train_model1.py
│   │   └── train_model2.py
│   ├── __init__.py
│   ├── config.py
│   └── utils.py
├── tests
│   ├── __init__.py
│   ├── test_feature_extractor.py
│   └── test_utils.py
├── .gitignore.py
├── .pre-commit-config.yaml
├── __init__.py
├── Dockerfile
├── README.md
└── requirements.txt
└── setup.py
```

<p align="right">(<a href="#top">back to top</a>)</p>
<br>

### Description

**The src folder**
contains the main functions used in the project to:
- prepare the data
- train the models
- evaluate the models
- make predictions with the models

**The app folder**
contains the file needed to:
- configure an API to serve the model
- package the appliation

<p align="right">(<a href="#top">back to top</a>)</p>
<br>

## Getting started

### Create environment
`conda create -m servier -python=3.6`<br>
`conda activate servier`<br>
`pip install -r requirements.txt`<br>
`conda install -c conda-forge rdkit`


### API

The model can be served through an API.
- To run it, execute the code `python app/api_app.py`
- Send a molecule smile to get a prediction <br>
ex: _URL/predict?smile=Cc1cccc(N2CCN(C(=O)C34CC5CC(CC(C5)C3)C4)CC2)c1C_
<br><br>

### Packaging

The application is installable through the app/setup.py file.<br><br>
To install it, run the command:
`python setup.py install`

After installing the package, the following commands are available:
- `prepare -n "<your-datafile>" -b <with_fingerprint>`: prepares the data for the next commands
  - the `datafile` must be a csv file with at least the rows "smiles" and "P1"
  - the boolean `with_fingerprint` determines wether or not to use fingerprinting 
  (Use `True` for model1 and `False` for model2)
  
- `train -t <type_of_model>`: trains a new model 
  - `type_of_model` takes 1 or 2
  
- `evaluate -t <type_of_model>`: evaluates the model 
  - `type_of_model` takes 1 or 2
  
- `predict -t <type_of_model> -n "<your-smile>"`: makes a prediction for a given molecule smiles

- `run_api`: runs the API (runs only on model 1)
<br><br>

### Docker image

The application is packaged in a Docker image.

The data is not included in the image and has to be mounted while running the image.
- To build the Docker image, run the command:
`docker build . -t servier`
<br><br>
- To run the image and mount a volume (here, the data to mount is in a data folder located in the current path):
`docker run --mount type=bind,source="$(pwd)"/data,target=/data -p 5000:5000 -it servier`
<br><br>
- The packaging commands are then available, and you can use them the same way.

<b>/!\ No models are integrated in the image.
You then have to prepare the data and train the model before using the other commands (including the run_api)</b>

<p align="right">(<a href="#top">back to top</a>)</p>


## Results

### Model 1
- Train: 
  - Loss: 0.1034
  - Accuracy: 0.9612
- Validation: 
  - Loss: 0.8377
  - Accuracy: 0.7772
- Test: 
  - Loss: 1.5679
  - Accuracy: 0.7824

### Model 2
- Train: 
  - Loss: 0.4137
  - Accuracy: 0.8221
- Validation: 
  - Loss: 0.4418
  - Accuracy: 0.8230
- Test: 
  - Loss: 0.6527
  - Accuracy: 0.8248
