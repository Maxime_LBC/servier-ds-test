FROM continuumio/miniconda3:latest

RUN conda init bash && \
    . /root/.bashrc && \
    conda update -n base conda && \
    conda create -y --name servier python=3.6 && \
    conda activate servier

RUN conda install -c conda-forge rdkit

# COPY SCRIPTS TO IMAGES
COPY /app /app
COPY /src /src
COPY requirements.txt ./
COPY setup.py ./

# INSTALL PIP AND REQUIREMENTS
RUN conda install -c anaconda pip
RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt

# EXPOSE PORT
EXPOSE 5000

# INSTALL SETUP
RUN python setup.py install
