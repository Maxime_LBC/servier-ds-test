from typing import List

import numpy as np
from tensorflow import keras


def get_vocabulary(data: np.ndarray) -> List[str]:
    """
    From data features, extract every distinct characters into a list
    Args:
        pd.DataFrame containing at least a column "features"
    Returns a list of distinct characters
    """

    # List all distinct characters
    list_char = list()
    for feat in data:
        char_in_feat = list(set(feat))
        list_char = list(set(list_char + char_in_feat))

    return list_char


def load_model(model_path: str) -> keras.Sequential:
    """
    Loads a model from a saved object
    Args:
        model_path: path to the saved model
    Returns a Keras model
    """
    model = keras.models.load_model(model_path, compile=False)

    model.compile(optimizer='adam',
                  loss=keras.losses.BinaryCrossentropy(from_logits=True),
                  metrics=['binary_accuracy'])

    return model
