import pickle

import numpy as np
from tensorflow.keras.preprocessing.text import Tokenizer

import src.config as config
from src.utils import get_vocabulary


def fit_tokenizer(data: np.ndarray) -> Tokenizer:
    """
    Fits a tokenizer on the data vocabulary and saves it
    Args
        data: array containing the text data
    Returns a fitted Tokenizer
    """
    tokenizer = Tokenizer(
        num_words=None, filters=None, lower=False, split=None, char_level=True
    )
    tokenizer.fit_on_texts(get_vocabulary(data))

    # Save tokenizer for predict
    with open(f'{config.MODEL_PATH}/tokenizer.pickle', 'wb') as handle:
        pickle.dump(tokenizer, handle)
    return tokenizer


def tokenize(data: np.ndarray, tokenizer: Tokenizer, max_len) -> np.ndarray:
    """
    Tokenize the input data thanks to a given tokenizer
    Args
        data: array containing the data
        tokenizer: pre-fitted tokenizer
        max_len: max length of data elements
    Returns an array containing the tokenized data
    """
    tokens = tokenizer.texts_to_sequences(data)

    for liste in tokens:
        while len(liste) < max_len:
            liste.append(0)

    return np.array(tokens)
