from typing import Tuple

from src.gathering.get_data import import_splitted_data
from src.preprocessing.tokenize import fit_tokenizer, tokenize
from src.utils import load_model


def evaluate_model_on_test(model_path: str) -> Tuple[float, float]:
    """
    Evaluates a model on a test set
    Args
        model_path: path to the saved model
    Returns a tuple of loss and accuracy values
    """
    _, _, X_test, y_test, _, _ = import_splitted_data()

    model = load_model(model_path)
    loss, acc = model.evaluate(X_test, y_test)
    return loss, acc
