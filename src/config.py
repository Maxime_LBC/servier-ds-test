
# Path of data
DATA_PATH = "data"
DATA_FILE = "dataset_single.csv"

# Important columns of data
FEATURE_NAME = "smiles"
TARGET_NAME = "P1"

# Model path
MODEL_PATH = "model"
