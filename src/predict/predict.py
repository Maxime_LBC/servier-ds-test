import pickle

import numpy as np

import src.config as config
from src.preprocessing.feature_extractor import fingerprint_features
from src.utils import load_model


def predict_property_smile(model_path: str, smiles: str) -> float:
    """
    Predicts the property P1 for a given smile, using the model1
    Args:
        model_path: path to the model
        smiles: string representation of a molecule
    Returns: prediction made by the model
    """
    # Calculate features
    features = fingerprint_features(smiles)

    # Load model
    model = load_model(model_path)
    X = np.array(list(features))

    # Predicts property
    prediction = model.predict(X.reshape(1, -1))
    return prediction


def predict_property_smile_model2(model_path: str, smiles: str) -> float:
    """
    Predicts the property P1 for a given smile using the model2
    Args:
        model_path: path to the model
        smiles: string representation of a molecule
    Returns: prediction made by the model
    """
    # Import tokenizer
    with open(f'../{config.MODEL_PATH}/tokenizer.pickle', 'rb') as handle:
        tokenizer = pickle.load(handle)

    # Load model
    model = load_model(model_path)
    tokens = tokenizer.texts_to_sequences(smiles)
    while len(tokens) < 74:
        tokens.append(0)

    X = np.array(tokens)

    # Predicts property
    prediction = model.predict(X.reshape(1, -1))
    return prediction[0][0]
