import numpy as np
from tensorflow import keras

import src.config as config
from src.gathering.get_data import import_splitted_data
from src.utils import get_vocabulary


def get_model(vocab_size: int, max_len: int) -> keras.Sequential:
    """
    Generates Keras Sequential  model
    Args:
        vocab_size: size of input vocabulary
        max_len: max length of input
    Returns a compiled Keras model
    """

    # Hyperparameters
    embedding_dim = 64
    lstm1_dim = 64
    lstm2_dim = 32
    dense_dim = 64

    model = keras.Sequential([
        keras.layers.Embedding(vocab_size+1, embedding_dim, input_length=max_len),
        keras.layers.Bidirectional(keras.layers.LSTM(lstm1_dim, return_sequences=True)),
        keras.layers.Bidirectional(keras.layers.LSTM(lstm2_dim)),
        keras.layers.Dense(dense_dim, activation='relu'),
        keras.layers.Dense(1, activation='sigmoid')
    ])

    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['binary_accuracy'])

    return model


def train(model: keras.Sequential,
          X_train: np.ndarray, y_train: np.ndarray,
          X_val: np.ndarray, y_val: np.ndarray) -> keras.callbacks.History:
    """
    Trains a Keras model
    Args:
        model: Keras model
        X_train: train features
        y_train: train labels
        X_val: validation features
        y_val: validation labels
    Returns model fitting history
    """

    history = model.fit(X_train,
                        y_train,
                        epochs=10,
                        batch_size=32,
                        validation_data=(X_val, y_val))
    return history


def main() -> keras.callbacks.History:
    """
    Imports data, tains the model and save it
    Returns a callbacks history
    """
    # Get data
    X_train, y_train, _, _, X_val, y_val = import_splitted_data()

    max_len = max(len(elt) for elt in X_train)
    vocab = get_vocabulary(X_train)
    vocab_size = len(vocab)

    # Get model
    model = get_model(vocab_size, max_len)

    # Train model
    history = train(model, X_train, y_train, X_val, y_val)

    # Save model
    model.save(f"{config.MODEL_PATH}/model2")

    return history


if __name__ == '__main__':
    main()
