import datetime
import os

import numpy as np
from tensorflow import keras

import src.config as config
from src.gathering.get_data import import_splitted_data


def get_model(input_dim: int) -> keras.Sequential:
    """
    Generates Keras Sequential  model
    Args:
        input_dim: dimension of input in first layer
    Returns a compiled Keras model
    """
    model = keras.Sequential([
        keras.layers.Dense(64, activation='relu', input_dim=input_dim),
        keras.layers.Dropout(0.5),
        keras.layers.Dense(32, activation='relu'),
        keras.layers.Dense(1, activation='sigmoid')
    ])

    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['binary_accuracy'])

    return model


def train(model: keras.Sequential,
          X_train: np.ndarray, y_train: np.ndarray,
          X_val: np.ndarray, y_val: np.ndarray) -> keras.callbacks.History:
    """
    Trains a Keras model
    Args:
        model: Keras model
        X_train: train features
        y_train: train labels
        X_val: validation features
        y_val: validation labels
    Returns model fitting history
    """

    log_dir = os.path.join("logs", "fit", datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

    # Fit model
    history = model.fit(X_train,
                        y_train,
                        epochs=10,
                        batch_size=32,
                        validation_data=(X_val, y_val),
                        callbacks=[tensorboard_callback])
    return history


def main() -> keras.callbacks.History:
    """
    Imports data, tains the model and save it
    Returns a callbacks history
    """
    # Get data
    X_train, y_train, _, _, X_val, y_val = import_splitted_data()

    # Get model
    model = get_model(X_train.shape[1])

    # Train model
    history = train(model, X_train, y_train, X_val, y_val)

    # Save model
    model.save(f"{config.MODEL_PATH}/model1")

    return history


if __name__ == '__main__':
    main()
