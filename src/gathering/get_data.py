from typing import Tuple

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

import src.config as config
from src.preprocessing.feature_extractor import fingerprint_features
from src.preprocessing.tokenize import fit_tokenizer, tokenize


def get_data(datafile: str,
             with_fingerprint: bool = True) -> pd.DataFrame:
    """
    Imports data and apply fingerprint features transformation
    Args:
        datafile: path of the csv containing the data
        with_fingerprint: whether to use fingerprint features transformation
    Returns a pandas DataFrame
    """
    data = pd.read_csv(datafile)

    if with_fingerprint:
        data['features'] = data[config.FEATURE_NAME].apply(lambda x: fingerprint_features(x))
    else:
        data.rename(columns={config.FEATURE_NAME: 'features'}, inplace=True)
    return data


def split_data(data: pd.DataFrame) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """
    Splits dataframe into 6 ndarrays (X, y for train, test and val sets)
    Args:
        data: dataframe containing at least "P1" and "features" columns
    Returns 6 ndarrays
    """
    # Convert to Numpy arrays
    X = np.array(list(data['features']))
    y = data[config.TARGET_NAME].values

    # Divide into train, test and val sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=42)
    X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.25, random_state=1)

    return X_train, y_train, X_test, y_test, X_val, y_val


def save_splitted_data(X_train: np.ndarray, y_train: np.ndarray,
                       X_test: np.ndarray, y_test: np.ndarray,
                       X_val: np.ndarray, y_val: np.ndarray,
                       output_file: str = f"{config.DATA_PATH}/splitted_data.npy") -> None:
    """
    Saves train, test and val sets (X and y) into a npy file
    """
    with open(output_file, 'wb') as f:
        for data in [X_train, y_train, X_test, y_test, X_val, y_val]:
            np.save(f, data)


def load_and_split_data(datafile: str,
                        with_fingerprint: bool = True) -> None:
    """
    Loads and splits data into 6 arrays (X and y for train, test and validation sets),
    and saves them in the data folder
    Args:
        datafile: path to the csv file containing at least "P1" and "smiles" columns
        with_fingerprint: whether to use fingerprint features transformation
    """
    data = get_data(datafile, with_fingerprint=with_fingerprint)
    X_train, y_train, X_test, y_test, X_val, y_val = split_data(data)

    if not with_fingerprint:
        max_len = max(len(elt) for elt in X_train)

        # Preprocess data
        tokenizer = fit_tokenizer(X_train)
        X_train = tokenize(X_train, tokenizer, max_len)
        X_test = tokenize(X_test, tokenizer, max_len)
        X_val = tokenize(X_val, tokenizer, max_len)

    save_splitted_data(X_train, y_train, X_test, y_test, X_val, y_val)


def import_splitted_data(data_path: str = f"{config.DATA_PATH}/splitted_data.npy") \
        -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    with open(data_path, 'rb') as f:
        X_train = np.load(f)
        y_train = np.load(f)
        X_test = np.load(f)
        y_test = np.load(f)
        X_val = np.load(f)
        y_val = np.load(f)

    return X_train, y_train, X_test, y_test, X_val, y_val
