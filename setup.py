from setuptools import setup

setup(
    name="servier",
    version="0.1.0",
    author='Maxime Villain',
    packages=["app", "src", "src.evaluate", "src.gathering",
              "src.predict", "src.preprocessing", "src.training"],
    entry_points={
        "console_scripts": [
            "prepare = app.main:prepare_data",
            "train = app.main:train_model",
            "evaluate = app.main:evaluate_model",
            "predict = app.main:predict",
            "run_api = app.main:run_api"
        ]
    },
    python_requires='==3.6',
)
