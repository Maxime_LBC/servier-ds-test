import argparse

import numpy as np

import src.config as config
from app.api_app import app
from src.evaluate.evaluate_model import evaluate_model_on_test
from src.gathering.get_data import load_and_split_data
from src.predict.predict import predict_property_smile
from src.training.train_model1 import main as train_model1
from src.training.train_model2 import main as train_model2


# Prepare the data
def prepare_data() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--datafile', action="store", help='Provides path to data file')
    parser.add_argument('-b', '--with_fingerprint', default=True,
                        action="store", help='Whether or not to use fingerprinting')
    args = parser.parse_args()

    print(f"Load the data file {args.datafile}")
    if args.with_fingerprint == 'True':
        load_and_split_data(args.datafile, with_fingerprint=True)
    else:
        load_and_split_data(args.datafile, with_fingerprint=False)
    print("The data has been splitted and saved successfully")


# Train the model
def train_model():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--type_of_model', default=1, type=int, action="store",
                        help='Provides type of model to train')
    args = parser.parse_args()

    print(f"Model {args.type_of_model} training")
    if args.type_of_model == 1:
        train_model1()
    elif args.type_of_model == 2:
        train_model2()
    else:
        print("WARNING: type_of_model must be 1 or 2")
        return
    print("Training of the model OK")


# Evaluate the model on an independent set
def evaluate_model():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--type_of_model', default=1, type=int,
                        action="store", help='Provides type of model to train')
    args = parser.parse_args()

    print("Evaluation of the model on a test set")
    loss, acc = evaluate_model_on_test(f'{config.MODEL_PATH}/model{args.type_of_model}')
    print(f"Loss: {loss}, Accuracy: {acc}")


# Predict the property P1 of a given smile
def predict():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--type_of_model', default=1, type=int,
                        action="store", help='Provides type of model to train')
    parser.add_argument('-n', '--smile', action="store", help='Provides smile to predict')
    args = parser.parse_args()

    prediction = predict_property_smile(f'{config.MODEL_PATH}/model{args.type_of_model}', args.smile)

    print(f"The prediction for {args.smile} is {np.round(prediction[0][0]*100, 2)}%")


def run_api():
    print("Launching the API")
    app.run(debug=False, host="0.0.0.0", port=5000)
