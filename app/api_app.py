import numpy as np
from flask import Flask, request

import src.config as config
from src.predict.predict import predict_property_smile

app = Flask(__name__)


@app.route("/", methods=['GET'])
def root():
    output = """<h1>Hello! </h1><br><br>
    The objective of this API is to predict the probability of P1 property for a given smile <br><br>
    To get a response, complete the URL with /predict?smile=your_smile <br><br>
    Example: <i>http://127.0.0.1:5000/predict?smile=Cc1cccc(N2CCN(C(=O)C34CC5CC(CC(C5)C3)C4)CC2)c1C</i>"""
    return output


@app.route("/predict", methods=['GET'])
def predict_property_molecule():
    smile = request.args.get('smile')
    prediction = predict_property_smile(f'{config.MODEL_PATH}/model1', smile)
    return f""" <h3> The prediction of P1 for {smile} is: {np.round(prediction[0][0]*100, 2)}% </h3> """


if __name__ == '__main__':
    app.run(debug=False, host="0.0.0.0", port=5000)
