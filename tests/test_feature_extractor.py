from src.preprocessing.feature_extractor import fingerprint_features


def test_fingerprint_features_dimension():
    features1 = fingerprint_features("Cc1cccc(N2CCN(C(=O)C34CC5CC(CC(C5)C3)C4)CC2)c1C")
    features2 = fingerprint_features("COc1ccccc1Nc1nc2c(s1)CCC2", size=1024)

    assert len(features1) == 2048
    assert len(features2) == 1024
