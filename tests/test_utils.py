from src.utils import get_vocabulary


def test_get_vocabulary():
    list1 = ['abcd','ABCD']
    list2 = ['aAbBcCdD','ABCD']
    list3 = ['(a=a)','ABCD']

    vocab1 = get_vocabulary(list1)
    vocab2 = get_vocabulary(list2)
    vocab3 = get_vocabulary(list3)

    assert vocab1.sort() == ['a', 'b', 'c', 'd', 'A', 'B', 'C', 'D'].sort()
    assert vocab2.sort() == ['a', 'b', 'c', 'd', 'A', 'B', 'C', 'D'].sort()
    assert vocab3.sort() == ['a', 'A', 'B', 'C', 'D', '(', '=', ')'].sort()
